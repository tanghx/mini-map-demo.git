(wx["webpackJsonp"] = wx["webpackJsonp"] || []).push([["pages/index/index"],{

/***/ "./node_modules/@babel/runtime/helpers/esm/objectSpread2.js":
/*!******************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/esm/objectSpread2.js ***!
  \******************************************************************/
/*! exports provided: default */
/*! exports used: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return _objectSpread2; });
/* harmony import */ var _defineProperty_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./defineProperty.js */ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js");

function ownKeys(e, r) {
  var t = Object.keys(e);
  if (Object.getOwnPropertySymbols) {
    var o = Object.getOwnPropertySymbols(e);
    r && (o = o.filter(function (r) {
      return Object.getOwnPropertyDescriptor(e, r).enumerable;
    })), t.push.apply(t, o);
  }
  return t;
}
function _objectSpread2(e) {
  for (var r = 1; r < arguments.length; r++) {
    var t = null != arguments[r] ? arguments[r] : {};
    r % 2 ? ownKeys(Object(t), !0).forEach(function (r) {
      Object(_defineProperty_js__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"])(e, r, t[r]);
    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) {
      Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r));
    });
  }
  return e;
}


/***/ }),

/***/ "./node_modules/@tarojs/taro-loader/lib/raw.js!./src/pages/index/index.vue":
/*!*********************************************************************************!*\
  !*** ./node_modules/@tarojs/taro-loader/lib/raw.js!./src/pages/index/index.vue ***!
  \*********************************************************************************/
/*! exports provided: default */
/*! exports used: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _index_vue_vue_type_template_id_1badc801__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.vue?vue&type=template&id=1badc801 */ "./src/pages/index/index.vue?vue&type=template&id=1badc801");
/* harmony import */ var _index_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.vue?vue&type=script&lang=js */ "./src/pages/index/index.vue?vue&type=script&lang=js");
/* harmony import */ var _Users_tanghx_Desktop_partner_mini_map_demo_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");
/* harmony import */ var _Users_tanghx_Desktop_partner_mini_map_demo_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_Users_tanghx_Desktop_partner_mini_map_demo_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__);





const __exports__ = /*#__PURE__*/_Users_tanghx_Desktop_partner_mini_map_demo_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2___default()(_index_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"], [['render',_index_vue_vue_type_template_id_1badc801__WEBPACK_IMPORTED_MODULE_0__[/* render */ "a"]],['__file',"src/pages/index/index.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["a"] = (__exports__);

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/vue-loader/dist/index.js?!./src/pages/index/index.vue?vue&type=script&lang=js":
/*!*************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib!./node_modules/vue-loader/dist??ref--10-0!./src/pages/index/index.vue?vue&type=script&lang=js ***!
  \*************************************************************************************************************************************/
/*! exports provided: default */
/*! exports used: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _Users_tanghx_Desktop_partner_mini_map_demo_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue */ "./node_modules/@vue/reactivity/dist/reactivity.esm-bundler.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vue */ "./node_modules/@vue/runtime-core/dist/runtime-core.esm-bundler.js");
/* harmony import */ var _tarojs_taro__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @tarojs/taro */ "./node_modules/@tarojs/taro/index.js");
/* harmony import */ var _tarojs_taro__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_tarojs_taro__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _index_scss__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./index.scss */ "./src/pages/index/index.scss");
/* harmony import */ var _index_scss__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_index_scss__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _images_location_png__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../images/location.png */ "./src/images/location.png");
/* harmony import */ var _images_location_png__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_images_location_png__WEBPACK_IMPORTED_MODULE_5__);





/* harmony default export */ __webpack_exports__["a"] = ({
  setup: function setup() {
    var setting = Object(vue__WEBPACK_IMPORTED_MODULE_1__[/* ref */ "o"])({
      scale: 16,
      maxScale: 20,
      minScale: 12
    });
    var markers = Object(vue__WEBPACK_IMPORTED_MODULE_1__[/* ref */ "o"])([]);
    var clusters = Object(vue__WEBPACK_IMPORTED_MODULE_1__[/* ref */ "o"])([]);
    var mapContext;
    var index = 0;
    Object(vue__WEBPACK_IMPORTED_MODULE_2__[/* onMounted */ "t"])(function () {
      mapContext = _tarojs_taro__WEBPACK_IMPORTED_MODULE_3___default.a.createMapContext('map');
      mapContext.initMarkerCluster({
        enableDefaultStyle: false
      });
      mapContext.addMarkers({
        markers: markers.value,
        clear: false
      });
      addMarkerClusterEvent();
    });
    var addMarkerClusterEvent = function addMarkerClusterEvent() {
      mapContext.on('markerClusterCreate', function (e) {
        console.log(111);
        clusters.value = e.clusters.map(function (item) {
          var center = item.center,
            clusterId = item.clusterId;
          return Object(_Users_tanghx_Desktop_partner_mini_map_demo_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"])(Object(_Users_tanghx_Desktop_partner_mini_map_demo_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"])({}, center), {}, {
            clusterId: clusterId,
            iconPath: _images_location_png__WEBPACK_IMPORTED_MODULE_5___default.a,
            width: 20,
            height: 20,
            customCallout: {
              display: 'ALWAYS',
              anchorY: -4
            }
          });
        });
        mapContext.addMarkers({
          markers: clusters.value,
          clear: false
        });
      });
    };
    var onAddMarker = function onAddMarker() {
      var marker = {
        id: index++,
        latitude: 30.41592035809696 + Math.random() * 0.006543,
        longitude: 120.29657659115685 + Math.random() * 0.003883,
        iconPath: _images_location_png__WEBPACK_IMPORTED_MODULE_5___default.a,
        joinCluster: true,
        width: 20,
        height: 20,
        customCallout: {
          display: 'ALWAYS',
          anchorY: -4
        }
      };
      markers.value.push(marker);
      mapContext.addMarkers({
        markers: markers.value,
        clear: false
      });
    };
    return {
      setting: setting,
      markers: markers,
      clusters: clusters,
      onAddMarker: onAddMarker
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/vue-loader/dist/templateLoader.js?!./node_modules/vue-loader/dist/index.js?!./src/pages/index/index.vue?vue&type=template&id=1badc801":
/*!****************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib!./node_modules/vue-loader/dist/templateLoader.js??ref--6!./node_modules/vue-loader/dist??ref--10-0!./src/pages/index/index.vue?vue&type=template&id=1badc801 ***!
  \****************************************************************************************************************************************************************************************************/
/*! exports provided: render */
/*! exports used: render */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return render; });
/* harmony import */ var _Users_tanghx_Desktop_partner_mini_map_demo_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/toConsumableArray */ "./node_modules/@babel/runtime/helpers/esm/toConsumableArray.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue */ "./node_modules/@vue/runtime-core/dist/runtime-core.esm-bundler.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vue */ "./node_modules/@vue/shared/dist/shared.esm-bundler.js");


var _hoisted_1 = {
  class: "home"
};
var _hoisted_2 = ["setting"];
var _hoisted_3 = {
  slot: "callout"
};
var _hoisted_4 = ["marker-id"];
var _hoisted_5 = {
  class: "marker-callout"
};
var _hoisted_6 = ["marker-id"];
function render(_ctx, _cache, $props, $setup, $data, $options) {
  return Object(vue__WEBPACK_IMPORTED_MODULE_1__[/* openBlock */ "w"])(), Object(vue__WEBPACK_IMPORTED_MODULE_1__[/* createElementBlock */ "h"])("view", _hoisted_1, [Object(vue__WEBPACK_IMPORTED_MODULE_1__[/* createElementVNode */ "i"])("map", {
    class: "home-map",
    id: "map",
    longitude: "120.298501",
    latitude: "30.41875",
    setting: $setup.setting,
    "show-location": true
  }, [Object(vue__WEBPACK_IMPORTED_MODULE_1__[/* createElementVNode */ "i"])("cover-view", _hoisted_3, [(Object(vue__WEBPACK_IMPORTED_MODULE_1__[/* openBlock */ "w"])(true), Object(vue__WEBPACK_IMPORTED_MODULE_1__[/* createElementBlock */ "h"])(vue__WEBPACK_IMPORTED_MODULE_1__[/* Fragment */ "c"], null, Object(vue__WEBPACK_IMPORTED_MODULE_1__[/* renderList */ "y"])($setup.markers, function (item) {
    return Object(vue__WEBPACK_IMPORTED_MODULE_1__[/* openBlock */ "w"])(), Object(vue__WEBPACK_IMPORTED_MODULE_1__[/* createElementBlock */ "h"])("cover-view", {
      "marker-id": item.id
    }, [Object(vue__WEBPACK_IMPORTED_MODULE_1__[/* createElementVNode */ "i"])("cover-view", _hoisted_5, " 这是自定义的marker" + Object(vue__WEBPACK_IMPORTED_MODULE_2__[/* toDisplayString */ "V"])(item.id), 1 /* TEXT */)], 8 /* PROPS */, _hoisted_4);
  }), 256 /* UNKEYED_FRAGMENT */)), (Object(vue__WEBPACK_IMPORTED_MODULE_1__[/* openBlock */ "w"])(true), Object(vue__WEBPACK_IMPORTED_MODULE_1__[/* createElementBlock */ "h"])(vue__WEBPACK_IMPORTED_MODULE_1__[/* Fragment */ "c"], null, Object(vue__WEBPACK_IMPORTED_MODULE_1__[/* renderList */ "y"])($setup.clusters, function (item) {
    return Object(vue__WEBPACK_IMPORTED_MODULE_1__[/* openBlock */ "w"])(), Object(vue__WEBPACK_IMPORTED_MODULE_1__[/* createElementBlock */ "h"])("cover-view", {
      "marker-id": item.clusterId
    }, Object(_Users_tanghx_Desktop_partner_mini_map_demo_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"])(_cache[1] || (_cache[1] = [Object(vue__WEBPACK_IMPORTED_MODULE_1__[/* createElementVNode */ "i"])("cover-view", {
      class: "marker-callout"
    }, " 这是自定义的cluster ", -1 /* HOISTED */)])), 8 /* PROPS */, _hoisted_6);
  }), 256 /* UNKEYED_FRAGMENT */))])], 8 /* PROPS */, _hoisted_2), Object(vue__WEBPACK_IMPORTED_MODULE_1__[/* createElementVNode */ "i"])("button", {
    style: {
      "position": "absolute",
      "bottom": "10px"
    },
    type: "primary",
    onTap: _cache[0] || (_cache[0] = function () {
      return $setup.onAddMarker && $setup.onAddMarker.apply($setup, arguments);
    })
  }, "点击添加marker", 32 /* NEED_HYDRATION */)]);
}

/***/ }),

/***/ "./node_modules/vue-loader/dist/exportHelper.js":
/*!******************************************************!*\
  !*** ./node_modules/vue-loader/dist/exportHelper.js ***!
  \******************************************************/
/*! no static exports found */
/*! exports used: default */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
// runtime helper for setting properties on components
// in a tree-shakable way
exports.default = (sfc, props) => {
    const target = sfc.__vccOpts || sfc;
    for (const [key, val] of props) {
        target[key] = val;
    }
    return target;
};


/***/ }),

/***/ "./src/images/location.png":
/*!*********************************!*\
  !*** ./src/images/location.png ***!
  \*********************************/
/*! no static exports found */
/*! exports used: default */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "images/location.png";

/***/ }),

/***/ "./src/pages/index/index.scss":
/*!************************************!*\
  !*** ./src/pages/index/index.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/pages/index/index.vue":
/*!***********************************!*\
  !*** ./src/pages/index/index.vue ***!
  \***********************************/
/*! no exports provided */
/*! all exports used */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _tarojs_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @tarojs/runtime */ "./node_modules/@tarojs/runtime/dist/runtime.esm.js");
/* harmony import */ var _node_modules_tarojs_taro_loader_lib_raw_js_index_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../node_modules/@tarojs/taro-loader/lib/raw.js!./index.vue */ "./node_modules/@tarojs/taro-loader/lib/raw.js!./src/pages/index/index.vue");


var config = {"navigationStyle":"custom"};


var inst = Page(Object(_tarojs_runtime__WEBPACK_IMPORTED_MODULE_0__["createPageConfig"])(_node_modules_tarojs_taro_loader_lib_raw_js_index_vue__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"], 'pages/index/index', {root:{cn:[]}}, config || {}))



/***/ }),

/***/ "./src/pages/index/index.vue?vue&type=script&lang=js":
/*!***********************************************************!*\
  !*** ./src/pages/index/index.vue?vue&type=script&lang=js ***!
  \***********************************************************/
/*! exports provided: default */
/*! exports used: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_dist_index_js_ref_10_0_index_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib!../../../node_modules/vue-loader/dist??ref--10-0!./index.vue?vue&type=script&lang=js */ "./node_modules/babel-loader/lib/index.js!./node_modules/vue-loader/dist/index.js?!./src/pages/index/index.vue?vue&type=script&lang=js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "a", function() { return _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_dist_index_js_ref_10_0_index_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_0__["a"]; });

 

/***/ }),

/***/ "./src/pages/index/index.vue?vue&type=template&id=1badc801":
/*!*****************************************************************!*\
  !*** ./src/pages/index/index.vue?vue&type=template&id=1badc801 ***!
  \*****************************************************************/
/*! exports provided: render */
/*! exports used: render */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_dist_templateLoader_js_ref_6_node_modules_vue_loader_dist_index_js_ref_10_0_index_vue_vue_type_template_id_1badc801__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib!../../../node_modules/vue-loader/dist/templateLoader.js??ref--6!../../../node_modules/vue-loader/dist??ref--10-0!./index.vue?vue&type=template&id=1badc801 */ "./node_modules/babel-loader/lib/index.js!./node_modules/vue-loader/dist/templateLoader.js?!./node_modules/vue-loader/dist/index.js?!./src/pages/index/index.vue?vue&type=template&id=1badc801");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "a", function() { return _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_dist_templateLoader_js_ref_6_node_modules_vue_loader_dist_index_js_ref_10_0_index_vue_vue_type_template_id_1badc801__WEBPACK_IMPORTED_MODULE_0__["a"]; });



/***/ })

},[["./src/pages/index/index.vue","runtime","taro","vendors"]]]);
//# sourceMappingURL=index.js.map